#include <iostream>

#include "persons.h"

using namespace std;

Employee::Employee(
    const char *last_name, const char *first_name, const char *middle_name, 
    const char *date_birth, const char *gender,
    const char *division, const char *post, double salary
): Human(last_name, first_name, middle_name, date_birth, gender)
{
    this->_division = division;
    this->_post = post;
    this->_salary = salary;
    std::cout<<"Создан сотрудник"<<endl;
    
    std::cout<<post<<" "<<salary<<" "<<division<<endl;
}

void Employee::hire()
{
    this->is_hired = true;
    std::cout<<"Сотрудника " + this->_last_name + " наняли на работу"<<endl;
}

void Employee::dismiss()
{
    this->is_hired = false;
    std::cout<<"Сотрудника " + this->_last_name + " уволили"<<endl;
}

bool Employee::get_is_hired() { return this->is_hired; }

double Employee::get_salary() { return this->_salary; }

std::string Employee::get_full_name() { return Human::get_full_name(); }

bool Employee::operator==(const Employee &employee) { return this->_salary == employee._salary; }

bool Employee::operator!=(const Employee &employee) { return this->_salary != employee._salary; }

bool Employee::operator>(const Employee &employee) { return this->_salary > employee._salary; }

bool Employee::operator<(const Employee &employee) { return this->_salary < employee._salary; }
