#ifndef ORDERS_H_
#define ORDERS_H_

#include <string>
#include <vector>

#include "products.h"

class Order
{
private:
    std::vector<Product*> _products;
    std::string _date;
    double _cost = 0;

public:
    Order();
    std::string get_date();
    double get_cost();
    std::vector<Product*> get_products();
    void add_product(Product *product);
};

#endif
