#include <iostream>
#include <vector>

#include "persons.h"
#include "orders.h"

using namespace std;


void Client::create_order(Order *order)
{
    this->_orders.push_back(order);
}

std::vector<Order*> Client::get_orders()
{
    return this->_orders;
}
