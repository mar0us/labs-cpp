#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

#include "console.h"
#include "persons.h"

using namespace std;


vector<Employee*> employees;

vector<Client*> clients;

vector<Product*> products;


class MainMenuItem: public AbstractMenuItem
{
public:
    void draw();
    AbstractMenuItem *select_item(char);
};


class EmployeesMenuItem: public AbstractMenuItem
{
private:
    void _hire();
    void _dismiss();
    void _compare_salary();
    void _show_all_employees();
    void _show_hired_employees();
    void _show_number_employees();
    void _calc_average_salary();
    void _calc_total_salary();

public:
    void draw();
    AbstractMenuItem *select_item(char);
};


class ConcreteClientMenuItem: public AbstractMenuItem
{
private:
    Client *_client;
    void _show_all_orders();
    void _create_new_order();
public:
    ConcreteClientMenuItem(Client *client);
    void draw();
    AbstractMenuItem *select_item(char);
};


class ClientsMenuItem: public AbstractMenuItem
{
private:
    void _create_client();
    void _write_clients_in_file();
    ConcreteClientMenuItem *_select_client();
public:
    void draw();
    AbstractMenuItem *select_item(char);
};


class ProductsMenuItem: public AbstractMenuItem
{
private:
    void _create_product();
    void _show_most_popular_product();
    void _show_all_products();
public:
    void draw();
    AbstractMenuItem *select_item(char);
};


class DialogMenuItem: public AbstractMenuItem
{
public:
    void draw()
    {
        cout<<"чтото"<<endl;
    }
    AbstractMenuItem *select_item(char item)
    {
        if (item == 'm')
            return new MainMenuItem();

        return this;
    }
};


Console::Console()
{
    cout<<"Конструктор консоли"<<endl;
}


Console::~Console()
{
    cout<<"Деструктор"<<endl;
}


void ClearConsole()
{
#if defined _WIN32
    system("cls");
#elif defined (__LINUX__) || defined(__gnu_linux__) || defined(__linux__)
    system("clear");
#elif defined (__APPLE__)
    system("clear");
#endif
}


void AwaitInput()
{
    cin.get(); cin.get();
}


void MainMenuItem::draw()
{
    cout<<"----------------------------------------"<<endl;
    cout<<"|            Главное меню              |"<<endl;
    cout<<"----------------------------------------"<<endl;
    cout<<"| 1. Сотрудники                        |"<<endl;
    cout<<"| 2. Клиенты                           |"<<endl;
    cout<<"| 3. Товары                            |"<<endl;
    cout<<"| e. Выход                             |"<<endl;
    cout<<"----------------------------------------"<<endl;
}


AbstractMenuItem *MainMenuItem::select_item(char item)
{
    switch (item)
    {
    case '1':
        return new EmployeesMenuItem();
        break;
    case '2':
        return new ClientsMenuItem();
        break;
    case '3':
        return new ProductsMenuItem();
        break;

    default:
        return this;
        break;
    }
}


void EmployeesMenuItem::_hire()
{
    string last_name, first_name, middle_name, date_birth, gender, division, post, salary;
    cout<<"Заполните данные сотрудника"<<endl; cin.get();
    cout<<"Фамилия: "; getline(cin, last_name);
    cout<<"Имя: "; getline(cin, first_name);
    cout<<"Отчество: "; getline(cin, middle_name);
    cout<<"Дата рождения: "; getline(cin, date_birth);
    cout<<"Пол: "; getline(cin, gender);
    cout<<"Подразделение: "; getline(cin, division);
    cout<<"Должность: "; getline(cin, post);
    cout<<"Зарплата: "; getline(cin, salary); cout<<endl;
    Employee *new_employee = new Employee(
        last_name.c_str(),
        first_name.c_str(),
        middle_name.c_str(),
        date_birth.c_str(),
        gender.c_str(),
        division.c_str(),
        post.c_str(),
        stod(salary)
    );
    new_employee->hire();
    employees.push_back(new_employee);
    cout<<new_employee->get_full_name()<<endl;
    cout<<"добавлен в список"<<endl;
}


void EmployeesMenuItem::_dismiss()
{
    // TODO: сделать защиту от обращения по существующему индексу
    this->_show_all_employees();
    int employee_index;
    cout<<"Введите номер сотрудника: "; cin >> employee_index; cout<<endl;
    employees[employee_index-1]->dismiss();
}


void EmployeesMenuItem::_compare_salary()
{
    this->_show_all_employees();
    int employee1_index, employee2_index;
    cout<<"Введите номер 1 сотрудника: "; cin >> employee1_index; cout<<endl;
    cout<<"Введите номер 2 сотрудника: "; cin >> employee2_index; cout<<endl;

    employee1_index--; employee2_index--;
    Employee *employee1 = employees[employee1_index];
    Employee *employee2 = employees[employee2_index];
    
    if (*employee1 > *employee2)
        cout<<"У сотрудника "<<employee1->get_full_name()<<" больше зарплата"<<endl;
    else if (*employee1 < *employee2)
        cout<<"У сотрудника "<<employee2->get_full_name()<<" больше зарплата"<<endl;
    else
        cout<<"Зарплаты равны"<<endl;
}


void EmployeesMenuItem::_show_all_employees()
{
    cout<<"Список всех сотрудников:"<<endl;
    for ( size_t i = 0; i < employees.size(); i++)
    {
        cout<<i+1<<". "<<employees[i]->_last_name<<endl;
    }
}


void EmployeesMenuItem::_show_hired_employees()
{
    cout<<"Список всех нанятых сотрудников:"<<endl;
    int i = 0;
    for ( auto employee : employees )
    {
        if (!employee->get_is_hired())
            continue;
            
        i++;
        cout<<i<<". "<<employee->_last_name<<endl;
    }
}


void EmployeesMenuItem::_show_number_employees()
{
    cout<<"На данный момент в штате "<<employees.size()<<" сотрудников"<<endl;
}


void EmployeesMenuItem::_calc_average_salary()
{
    double average_salary = 0;  
    for ( auto& employee : employees)
    {
        average_salary += employee->get_salary();
    }
    average_salary /= employees.size();
    cout<<"Средняя зарплата сотрудников: "<<average_salary<<endl;
}


void EmployeesMenuItem::_calc_total_salary()
{
    double total_salary = 0;
    for ( auto& employee : employees)
    {
        total_salary += employee->get_salary();
    }
    cout<<"Суммарная зарплата сотрудников: "<<total_salary<<endl;
}


void EmployeesMenuItem::draw()
{
    cout<<"----------------------------------------"<<endl;
    cout<<"|            Сотрудники                |"<<endl;
    cout<<"----------------------------------------"<<endl;
    cout<<"| 1. Нанять сотрудника                 |"<<endl;
    cout<<"| 2. Уволить сотрудника                |"<<endl;
    cout<<"| 3. Сравнить зарплату сотрудников     |"<<endl;
    cout<<"| 4. Вывести количество сотрудников    |"<<endl;
    cout<<"| 5. Вывести всех сотрудников          |"<<endl;
    cout<<"| 6. Посчитать среднюю зарплату        |"<<endl;
    cout<<"| 7. Посчитать суммарную зарплату      |"<<endl;
    cout<<"| 8. Вывести нанятых сотрудников       |"<<endl;
    cout<<"| m. Вернуться в меню                  |"<<endl;
    cout<<"| e. Выход                             |"<<endl;
    cout<<"----------------------------------------"<<endl;
}

AbstractMenuItem *EmployeesMenuItem::select_item(char item)
{
    AbstractMenuItem *menu_item;
    ClearConsole();
    switch (item)
    {
    case '1':
        this->_hire();
        menu_item = new EmployeesMenuItem();
        break;
    case '2':
        this->_dismiss();
        menu_item = new EmployeesMenuItem();
        break;
    case '3':
        this->_compare_salary();
        menu_item = new EmployeesMenuItem();
        break;
    case '4':
        this->_show_number_employees();
        menu_item = new EmployeesMenuItem();
        break;
    case '5':
        this->_show_all_employees();
        menu_item = new EmployeesMenuItem();
        break;
    case '6':
        this->_calc_average_salary();
        menu_item = new EmployeesMenuItem();
        break;
    case '7':
        this->_calc_total_salary();
        menu_item = new EmployeesMenuItem();
        break;
    case '8':
        this->_show_hired_employees();
        menu_item = new EmployeesMenuItem();
        break;
    case 'm':
        return new MainMenuItem();
        break;
    
    default:
        return this;
        break;
    }
    AwaitInput();
    return menu_item;
}


void ClientsMenuItem::_create_client()
{
    string last_name, first_name, middle_name, date_birth, gender, division, post, salary;
    cout<<"Заполните данные сотрудника"<<endl; cin.get();
    cout<<"Фамилия: "; getline(cin, last_name);
    cout<<"Имя: "; getline(cin, first_name);
    cout<<"Отчество: "; getline(cin, middle_name);
    cout<<"Дата рождения: "; getline(cin, date_birth);
    cout<<"Пол: "; getline(cin, gender);
    Client *new_client = new Client(
        last_name.c_str(),
        first_name.c_str(),
        middle_name.c_str(),
        date_birth.c_str(),
        gender.c_str()
    );
    clients.push_back(new_client);
    cout<<new_client->get_full_name()<<endl;
    cout<<"Клиент добавлен в список"<<endl;
}


void ClientsMenuItem::_write_clients_in_file()
{
    cout<<"Клиенты записаны в файл"<<endl;
    ofstream client_file;
    client_file.open("./clients.txt");
    if ( !client_file.is_open() )
    {
        cout<<"Файл не удалось открыть"<<endl;
        return;
    }

    int i = 1, j = 1, k = 1;
    for ( auto client : clients )
    {
        j = 1;
        client_file<<i<<". "<<client->get_full_name()<<endl;
        for ( auto order : client->get_orders() )
        {
            k = 1;
            client_file<<"\tЗаказ №"<<j<<". "<<endl;
            client_file<<"\t\tДата заказа: "<<order->get_date()<<endl;
            client_file<<"\t\tСтоимость: "<<order->get_cost()<<endl;
            j++;
            for ( auto product : order->get_products() )
            {
                client_file<<"\t\t\tТовар №"<<k<<". "<<endl;
                client_file<<"\t\t\t"<<product->get_info()<<endl;
                k++;
            }
        }
        i++;
    }
}


ConcreteClientMenuItem *ClientsMenuItem::_select_client()
{
    int i = 1;
    for ( auto client : clients )
    {
        cout<<i<<". "<<client->get_full_name()<<endl;
        i++;
    }
    int client_index;
    cout<<"Номер клиента: "; cin>>client_index;
    return new ConcreteClientMenuItem(clients[client_index-1]);
}


void ClientsMenuItem::draw()
{
    cout<<"----------------------------------------"<<endl;
    cout<<"|               Клиенты                |"<<endl;
    cout<<"----------------------------------------"<<endl;
    cout<<"| 1. Создать клиента                   |"<<endl;
    cout<<"| 2. Записать клиентов в файл          |"<<endl;
    cout<<"| 3. Выбрать клиента                   |"<<endl;
    cout<<"| m. Вернуться в меню                  |"<<endl;
    cout<<"| e. Выход                             |"<<endl;
    cout<<"----------------------------------------"<<endl;
}

AbstractMenuItem *ClientsMenuItem::select_item(char item)
{
    ClearConsole();
    switch (item)
    {
    case '1':
        this->_create_client();
        return this;
        break;
    case '2':
        this->_write_clients_in_file();
        return this;
        break;
    case '3':
        return this->_select_client();
        break;
    case 'm':
        return new MainMenuItem();
        break;
    
    default:
        return this;
        break;
    }
}


void ConcreteClientMenuItem::_show_all_orders()
{
    int i = 1;
    for ( auto order : this->_client->get_orders() )
    {
        cout<<"Заказ №"<<i<<". "<<endl;
        cout<<"\tДата заказа: "<<order->get_date()<<endl;
        cout<<"\tСтоимость: "<<order->get_cost()<<endl;
        int j = 1;
        for ( auto product : order->get_products() )
        {
            cout<<"\t\tТовар №"<<j<<". "<<endl;
            cout<<"\t\t"<<product->get_info()<<endl;
            j++;
        }
        i++;
    }
}


void ConcreteClientMenuItem::_create_new_order()
{
    cout<<"Введите номера товаров(каждый с новой строки)\nдля завершения выбора товаров введите -1"<<endl<<endl;
    Order *new_order = new Order();
    int i = 0;
    for ( auto product : products )
    {
        i++;
        cout<<i<<". "<<product->get_info()<<endl;
    }

    int product_index = 0;
    while(true)
    {
        cout<<"Номер товара: "; cin>>product_index;
        if (product_index == -1)
            break;

        new_order->add_product(products[product_index-1]);
    }

    this->_client->create_order(new_order);
}


ConcreteClientMenuItem::ConcreteClientMenuItem(Client *client)
{
    this->_client = client;
}


void ConcreteClientMenuItem::draw()
{
    cout<<"----------------------------------------"<<endl;
    cout<<"|               Клиент                 |"<<endl;
    cout<<"----------------------------------------"<<endl;
    cout<<"| 1. Вывести список заказов            |"<<endl;
    cout<<"| 2. Создать новый заказ               |"<<endl;
    cout<<"| m. Вернуться в меню                  |"<<endl;
    cout<<"| e. Выход                             |"<<endl;
    cout<<"----------------------------------------"<<endl;
}


AbstractMenuItem *ConcreteClientMenuItem::select_item(char item)
{
    ClearConsole();
    switch (item)
    {
    case '1':
        this->_show_all_orders();
        AwaitInput();
        return this;
        break;
    case '2':
        this->_create_new_order();
        return this;
        break;
    case 'm':
        return new MainMenuItem();
        break;
    
    default:
        return this;
        break;
    }
}


void ProductsMenuItem::_create_product()
{
    string name, date;
    double cost;
    cout<<"Введите данные товара: "<<endl; cin.get();
    cout<<"Наименование: "; getline(cin, name);
    cout<<"Дата производства: "; getline(cin, date);
    cout<<"Стоимость: "; cin>>cost;
    Product *new_product = new Product(
        name.c_str(),
        date.c_str(),
        cost
    );
    products.push_back(new_product);
}


void ProductsMenuItem::_show_most_popular_product()
{
    vector<Product*> products_in_orders;
    for ( auto client : clients )
    {
        for ( auto order : client->get_orders() )
        {
            vector<Product*> client_products = order->get_products();
            products_in_orders.reserve(products_in_orders.size() + distance(client_products.begin(), client_products.end()));
            products_in_orders.insert(products_in_orders.end(), client_products.begin(), client_products.end());
        }
    }

    int max_number_repetitions = 0;
    Product *max_product;
    for ( auto product : products )
    {
        int number_repetitions = count(products_in_orders.begin(), products_in_orders.end(), product);
        if ( number_repetitions >= max_number_repetitions )
        {
            max_number_repetitions = number_repetitions;
            max_product = product;
        }
    }

    if ( max_number_repetitions == 0)
        cout<<"Самого популярного товара нет"<<endl;
    else
        cout<<"Самый популярный товар: "<<max_product->get_info()<<endl;
}


void ProductsMenuItem::_show_all_products()
{
    int i = 0;
    for ( auto product : products )
    {
        i++;
        cout<<i<<". "<<product->get_info()<<endl;
    }
}


void ProductsMenuItem::draw()
{
    cout<<"----------------------------------------"<<endl;
    cout<<"|               Товары                 |"<<endl;
    cout<<"----------------------------------------"<<endl;
    cout<<"| 1. Создать товар                     |"<<endl;
    cout<<"| 2. Вывести самый популярный товар    |"<<endl;
    cout<<"| 3. Вывести список всех товаров       |"<<endl;
    cout<<"| m. Вернуться в меню                  |"<<endl;
    cout<<"| e. Выход                             |"<<endl;
    cout<<"----------------------------------------"<<endl;
}

AbstractMenuItem *ProductsMenuItem::select_item(char item)
{
    ClearConsole();
    switch (item)
    {
    case '1':
        this->_create_product();
        break;
    case '2':
        this->_show_most_popular_product();
        break;
    case '3':
        this->_show_all_products();
        break;
    case 'm':
        return new MainMenuItem();
        break;
    
    default:
        return this;
        break;
    }

    AwaitInput();
    return this;
}


void Console::loop()
{
    Product *product = new Product("Телефон", "17.03.2000", 2000);
    products.push_back(product);
    AbstractMenuItem *menu_item = new MainMenuItem();
    menu_item->draw();
    bool is_running = true;
    
    while (is_running)
    {
        char key = cin.get();
        if (key == 'e')
            is_running = false;

        menu_item = menu_item->select_item(key);
        ClearConsole();
        menu_item->draw();
    }
}