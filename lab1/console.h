#ifndef CONSOLE_H_
#define CONSOLE_H_

#include <string>


class AbstractMenuItem
{
public:
    virtual void draw() = 0;
    virtual AbstractMenuItem *select_item(char) = 0;
};


class Console
{
private:

public:
    Console();
    ~Console();
    void loop();
};

#endif
