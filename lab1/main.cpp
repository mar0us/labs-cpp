#include <iostream>

#include "products.h"
#include "orders.h"
#include "persons.h"
#include "console.h"

int main()
{
    Console *console = new Console();
    console->loop();

    delete console;

    return 0;
}