#include <iostream>

#include "products.h"

using namespace std;


Product::Product(const char *name, const char *date, double cost)
{
    this->_name = name;
    this->_date = date;
    this->_cost = cost;
}

double Product::get_cost()
{
    return this->_cost;
}

string Product::get_name()
{
    return this->_name;
}

string Product::get_info()
{
    string info = "Имя товара: " + this->_name + ", дата изготовления: " + this->_date + ", стоимость: " + to_string(this->_cost);
    return info;
}