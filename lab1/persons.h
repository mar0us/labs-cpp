#ifndef PERSONS_H_
#define PERSONS_H_

#include <string>
#include <vector>

#include "orders.h"

class Human
{
public:
    std::string _first_name;
    std::string _last_name;
    std::string _middle_name;
    std::string _date_birth;
    std::string _gender;
    Human(
        const char *first_name,
        const char *last_name,
        const char *middle_name,
        const char *date_birth,
        const char *gender
    );

    std::string get_full_name();
};


class Employee: public Human
{
private:
    bool is_hired = false;
    std::string _division;
    std::string _post;
    double _salary = 0;

public:
    Employee(
        const char *first_name,
        const char *last_name,
        const char *middle_name,
        const char *date_birth,
        const char *gender,
        const char *division,
        const char *post,
        double salary
    );
    bool get_is_hired();
    double get_salary();
    void hire();
    void dismiss();
    std::string get_full_name();

    bool operator==(const Employee &employee);
    bool operator!=(const Employee &employee);
    bool operator>(const Employee &employee);
    bool operator<(const Employee &employee);

};


class Client: public Human
{
private:
    std::vector<Order*> _orders = {};

public:
    using Human::Human;
    void create_order(Order *order);
    std::vector<Order*> get_orders();
};

#endif
