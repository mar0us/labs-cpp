#include <iostream>
#include <chrono>

#include "orders.h"

using namespace std;

Order::Order()
{
    auto now = std::chrono::system_clock::now();
    std::time_t end_time = std::chrono::system_clock::to_time_t(now);
    this->_date = std::ctime(&end_time);
}

void Order::add_product(Product *product)
{
    this->_products.push_back(product);
    this->_cost += product->get_cost();
}

std::vector<Product*> Order::get_products()
{
    return this->_products;
}

std::string Order::get_date()
{
    return this->_date;
}

double Order::get_cost()
{
    return this->_cost;
}
