#ifndef PRODUCTS_H_
#define PRODUCTS_H_

#include <string>
#include <list>

class Product
{
private:
    std::string _name;
    std::string _date;
    double _cost;

public:
    Product(const char *name, const char *date, double cost);
    double get_cost();
    std::string get_name();
    std::string get_info();
};

#endif
