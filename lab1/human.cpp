#include <iostream>

#include "persons.h"

using namespace std;


Human::Human(const char *last_name, const char *first_name, const char *middle_name, const char *date_birth, const char *gender)
{
    this->_last_name = last_name;
    this->_first_name = first_name;
    this->_middle_name = middle_name;
    this->_date_birth = date_birth;
    this->_gender = gender;
    std::cout<<"создан человек"<<endl;
    std::cout<<first_name<<" "<<last_name<<" "<<middle_name<<endl;
    std::cout<<date_birth<<" "<<gender<<endl;
}

string Human::get_full_name()
{
    string full_name = this->_last_name + " " + this->_first_name + " " + this->_middle_name;
    return full_name;
}
